export default () => ({
  users:[
    {
        id: 1,
        name: "sangpt",
        image: "sang.jpg",
        visible: false,
        online: false,
        data: [
            {
                id: 2,
                time: "Sep 20, 2020, 9:03 AM",
                content: "hello world !!",
            },
            {
                id: 2,
                time: "Sep 21, 2020, 19:03 AM",
                content: "hi cc ",
            },
            {
                id: 3,
                time: "Sep 30, 2020, 9:03 AM",
                content: "ok",
            },
        ]
    },
    {
        id: 2,
        name: "Van Cuong",
        image: "sang.jpg",
        visible: false,
        online: true,
        data: [
            {
                id: 3,
                time: "Sep 20, 2020, 9:03 AM",
                content: "hello sd world !!",
            },
            {
                id: 1,
                time: "Sep 21, 2020, 19:03 AM",
                content: "hi gv cc ",
            },
            {
                id: 3,
                time: "Sep 30, 2020, 9:03 AM",
                content: "ok",
            },
        ]
    },
    {
        id: 3,
        name: "Xuan Toan",
        image: "sang.jpg",
        visible: false,
        online: true,
        data: [
            {
                id: 2,
                time: "Sep 20, 2020, 9:03 AM",
                content: "h sdello world !!",
            },
            {
                id: 2,
                time: "Sep 21, 2020, 19:03 AM",
                content: "hi g cc ",
            },
            {
                id: 1,
                time: "Sep 30, 2020, 9:03 AM",
                content: "ok ghg ghg ghg ",
            },
        ]
    }
  ],
  oneUser: [
      {
        id: '',
        name: "",
        image: "",
        visible: true,
        online: true,
        data: []
      }
  ]
})