const express = require('express');
const router = express.Router();
const LoginController = require('../src/app/controller/LoginController');
router.post('/login', LoginController.login);
router.post('/register', LoginController.register);
router.get('/welcome', LoginController.welcome);
router.post('/refresh', LoginController.refresh);
module.exports = router;