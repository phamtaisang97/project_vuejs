const categoryRoute = require('./category');
const productRoute = require('./product');
const apiLogin = require('./api-login');

function route(app) {
    //category
    app.use('/danh-muc', categoryRoute);

    //product
    app.use('/san-pham', productRoute);

    //login
    app.use('/api-login', apiLogin);
}

module.exports = route;