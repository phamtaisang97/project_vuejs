const express = require('express');
const router = express.Router();
const ProductController = require('../src/app/controller/ProductController');
router.get('/', ProductController.index);
router.get('/them-moi', ProductController.create);
module.exports = router;