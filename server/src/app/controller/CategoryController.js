const Category = require('../models/Category')
const Products = require('../models/Product')
const db = require('mongoose');
class CategoryController {
    index(req, res) {
        Category.find({}, function(err, result) {
            if (err) {
            res.send(err);
            } else {
            res.json(result);
            }
        });
    }

    async getProductByCategory(req, res) {
        var data = await Products.find({});
        res.json(data);
    }

    async showProductByCategory(req, res) {
        var data = await Category.find({}).populate("products");
        res.json(data);

    }

    create(req, res) {
        Category.insertMany(
            [
              {
                name: "Phone",
              },
              {
                name: "PC",
              },
              {
                name: "Lap",
              },
            
            ],
            function(err, result) {
              if (err) {
                res.send(err);
              } else {
                res.json(result);
              }
            }
          );
    }
}

module.exports = new CategoryController;