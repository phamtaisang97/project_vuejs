const Users = require('../models/User')
const Products = require('../models/Product')

class UserController {
    index(req, res) {
        Users.find({}, function(err, result) {
            if (err) {
            res.send(err);
            } else {
            res.json(result);
            }
        });
    }

    show(req, res) {
        res.render("index");
    }

    create(req, res) {
        // Users.insertMany(
        //     [
        //       {
        //         name: "Leonel Messi",
        //         Country: "Argentina"
        //       },
        //       {
        //         name: "Cristiano Ronaldo",
        //         Country: "Portugal"
        //       },
        //       {
        //         name: "Neymar",
        //         Country: "Brazil"
        //       }
        //     ],
        //     function(err, result) {
        //       if (err) {
        //         res.send(err);
        //       } else {
        //         res.json(result);
        //       }
        //     }
        //   );
        Products.insertMany(
            [
              {
                name: "Nokia 1280",
                price: 123000,
                description: "Argentina Argentina Argentina Argentina",
                images: "1.jpg"
              },
              {
                name: "Samsung G1",
                price: 53000,
                description: "Good good",
                images: "1.jpg"
              },
              {
                name: "Iphone 12",
                price: 789000,
                description: "nice !!!",
                images: "1.jpg"
              },
            
            ],
            function(err, result) {
              if (err) {
                res.send(err);
              } else {
                res.json(result);
              }
            }
          );
    }
}

module.exports = new UserController;